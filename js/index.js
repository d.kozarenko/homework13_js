"use strict";

let button = document.createElement("button");
let anotherTheme = document.createElement(`link`);

localStorage.setItem("anotherCSSFile", "./css/aqua-theme.css");

buttonConstructor();
pageCheck();

function buttonConstructor() {
  button.textContent = "Aqua theme";
  let headerLogo = document.querySelector(".header-logo");
  headerLogo.append(button);
  button.classList.add("theme_button");
  button.addEventListener("click", themeChangingHandler);
}

function pageCheck() {
  if (localStorage.getItem("another theme")) {
    alternativeTheme();
    button.classList.add("button-active");
  }
}

function themeChangingHandler() {
  if (!button.classList.contains("button-active")) {
    button.classList.add("button-active");
    alternativeTheme();
    localStorage.setItem("another theme", "active");
    return;
  }
  defaultTheme();
}

function alternativeTheme() {
  anotherTheme.setAttribute("href", localStorage.getItem("anotherCSSFile"));
  anotherTheme.setAttribute("rel", "stylesheet");
  document.head.append(anotherTheme);
}

function defaultTheme() {
  button.classList.remove("button-active");
  anotherTheme.remove();
  localStorage.removeItem("another theme", "active");
}
